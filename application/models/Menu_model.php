<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu_model extends CI_Model
{
    public function getSubMenu()
    {
        $query = "SELECT `user_sub_menu`.*, `user_menu`.`menu`
                  FROM `user_sub_menu` JOIN `user_menu`
                  ON `user_sub_menu`.`menu_id` = `user_menu`.`id`";
        return $this->db->query($query)->result_array();
    }
    function get_all_data()
    {
        return $this->db->get('user')->result_array();
    }
    function simpan_data($image_name)
    {
        $data = array(
            'qr_code'   => $image_name
        );
        $this->db->insert('user', $data);
    }
    function barang_list()
    {
        // $hasil = $this->db->query("SELECT * FROM user_menu");
        // return $hasil->result();
        $hasil = $this->db->query("SELECT * FROM tbl_barang");
        $data = $hasil->result();
        echo json_encode($data);
    }

    // public function delete()
    // {
    //     return $this->db->delete('user_sub_menu', array("id" => $id));
    // }
}
