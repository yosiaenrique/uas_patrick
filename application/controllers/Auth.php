<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation', 'Recaptcha');
        $this->load->model('Menu_model');
    }

    public function index()
    {
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $recaptcha = $this->input->post('g-recaptcha-response');
        $response = $this->recaptcha->verifyResponse($recaptcha);

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Login Page';
            $data = array(
                'captcha' => $this->recaptcha->getWidget(),
                'script_captcha' => $this->recaptcha->getScriptTag(),
            );
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/login', $data);
            $this->load->view('templates/auth_footer');
        } else {
            // validasinya sukses
            if (!isset($response['success']) || $response['success'] <>  true) {
                $data['title'] = 'Login Page';
                $data = array(
                    'captcha' => $this->recaptcha->getWidget(),
                    'script_captcha' => $this->recaptcha->getScriptTag(),
                );
                $this->load->view('templates/auth_header', $data);
                $this->load->view('auth/login', $data);
                $this->load->view('templates/auth_footer');
            } else {
                $this->_login();
            }
        }
    }

    // public function login()
    // {
    //     $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
    //     $this->form_validation->set_rules('password', 'Password', 'trim|required');
    //     if ($this->form_validation->run() == false) {
    //         $data['title'] = 'Login';

    //         $this->load->view('templates/auth_header', $data);
    //         $this->load->view('auth/login');
    //         $this->load->view('templates/auth_footer');
    //     } else {
    //         $this->_login();
    //     }
    // }

    private function _login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $recaptcha = $this->input->post('g-recaptcha-response');
        $user = $this->db->get_where('user', ['email' => $email])->row_array();
        $response = $this->recaptcha->verifyResponse($recaptcha);

        //usernya ada
        if ($user) {
            //jika user aktif
            if ($user['is_active'] == 1) {
                // cek password
                if (password_verify($password, $user['password'])) {
                    $data = [
                        'email' => $user['email'],
                        'role_id' => $user['role_id']
                    ];
                    $this->session->set_userdata($data);
                    if ($user['role_id'] == 1) {
                        redirect('admin');
                    } else {
                        redirect('user');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Wrong password! </div>');
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> This email has not been activated! </div>');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Email is not registered! </div>');
            redirect('auth');
        }
    }

    public function registration()
    {
        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
            'is_unique' => 'This Email has already registerd!'
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[8]|matches[password2]', [
            'matches' => 'password  dont match!',
            'min_length' => 'Password too short!'
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'User Registration';
            $this->load->view('templates/auth_header2', $data);
            $this->load->view('auth/registration');
            $this->load->view('templates/auth_footer');
        } else {
            $id = $this->input->post('name');
            $isi = $id . "   " . $this->input->post('email');
            $this->load->library('ciqrcode'); //pemanggilan library QR CODE

            $config['cacheable']    = true; //boolean, the default is true
            $config['cachedir']     = './assets/'; //string, the default is application/cache/
            $config['errorlog']     = './assets/'; //string, the default is application/logs/
            $config['imagedir']     = './assets/images/'; //direktori penyimpanan qr code
            $config['quality']      = true; //boolean, the default is true
            $config['size']         = '1024'; //interger, the default is 1024
            $config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
            $config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
            $this->ciqrcode->initialize($config);

            $image_name = $id . '.png'; //buat name dari qr code sesuai dengan nim

            $params['data'] = $isi; //data yang akan di jadikan QR CODE
            $params['level'] = 'H'; //H=High
            $params['size'] = 10;
            $params['savename'] = FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
            $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

            $data = [
                'name' => htmlspecialchars($this->input->post('name', true)),
                'email' => htmlspecialchars($this->input->post('email', true)),
                'image' => 'default.jpg',
                'qr_code' => $image_name,
                'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'role_id' => 2,
                'is_active' => 1,
                'date_created' => time()
            ];

            $this->db->insert('user', $data);

            // $this->_sendEmail();

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Congratulation! your account has been created. Please Login </div>');
            redirect('auth');
        }
    }


    function send()
    {
        $email = $this->input->post('emailuser');
        $datauser = $this->Menu_model->get_all_data();
        $batas = count($datauser);
        for ($i = 0; $i < $batas; $i++) {
            if ($email == $datauser[$i]['email']) {
                $status = true;
                $id = $datauser[$i]['id'];
                break;
            }
        }

        if ($status) {
            // Load PHPMailer library
            $this->load->library('phpmailer_lib');

            // PHPMailer object
            $mail = $this->phpmailer_lib->load();

            // SMTP configuration
            $mail->isSMTP();
            $mail->Host     = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'enriqueyosia@gmail.com';
            $mail->Password = 'likval21';
            $mail->SMTPSecure = 'tls';
            $mail->Port     = 587;

            $mail->setFrom('enriqueyosia@gmail.com', 'MaYoDa');
            $mail->addReplyTo('enriqueyosia@gmail.com', 'MaYoDa');

            // Add a recipient
            $mail->addAddress($email);

            // Add cc or bcc 
            $mail->addCC('cc@example.com');
            $mail->addBCC('bcc@example.com');

            // Email subject
            $mail->Subject = 'Send Email via SMTP using PHPMailer in CodeIgniter';

            // Set email format to HTML
            $mail->isHTML(true);

            // Email body content
            $mailContent = "<h1>Send HTML Email using SMTP in CodeIgniter</h1>
            <p>This is a test email sending using SMTP mail server with PHPMailer.</p>";
            $mail->Body = $mailContent;

            // Send email
            if (!$mail->send()) {
                echo 'Message could not be sent.';
                echo 'Mailer Error: ' . $mail->ErrorInfo;
            } else {
                echo 'Message has been sent';
            }
        } else {
            redirect('auth');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role_id');

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            You has been logout </div>');
        redirect('auth');
    }
}
