-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 05, 2019 at 02:35 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wpu_login`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `image` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL,
  `qr_code` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`, `qr_code`) VALUES
(3, 'Like', 'likevalencia21@gmail.com', 'default.jpg', '$2y$10$N7LIbjxf5hbJPK31UtYf.utLil.YsabErlxN1du/.9deNZQ/mP23.', 1, 1, 1561253500, ''),
(4, 'Yosia', 'yosiaenrique0@gmail.com', 'default.jpg', '$2y$10$ZV6rn4TopG.Mmd4IN4QL.OKBjAUXKbbJvkZbAfviOySxO7sPtJSU6', 2, 1, 1561253515, ''),
(5, 'Daffa', 'daffa@gmail.com', 'default.jpg', '$2y$10$8kqJr9lV0ipaU3GIS7GcFey7zGJcFz1NHM40/Vipvwb6YNthrPhHa', 2, 1, 1561259243, ''),
(6, 'Malvin', 'malvin@gmail.com', 'default.jpg', '$2y$10$DBOr2LB1/H/kbdlkN07v0eUtBSSSMWZOjl7QNdKPTa7QOqSUsKb2W', 2, 0, 1561647880, ''),
(7, 'Ferry', 'ferrychai@gmail.com', 'default.jpg', '$2y$10$iKOI1hVew1x701w6TrH2/.lfJzVjaHp3sB0rFsQ/5MDAjO6PF819i', 2, 1, 1561771415, ''),
(8, 'sel', 'sel@gmail.com', 'default.jpg', '$2y$10$Ffk9dMmmaIKf2p/x0w1k0Oe0cculwI7V1xYZWK7xU7GSDL6wEtLz.', 2, 1, 1561794882, ''),
(9, 'sel', 'sel1@gmail.com', 'default.jpg', '$2y$10$j0Pwdi3qO0ODobDHDh1sduurDniKhE.UGzR/DEe295Ghv1GPciT6.', 2, 1, 1561794965, ''),
(10, 'aaaa', 'aaa@gmail.com', 'default.jpg', '$2y$10$FlToVMJAk0JqoHLPHZgR3Ox4TxahQKdlCSnzc8Nte1SHWKz9G0ttW', 2, 1, 1561796200, ''),
(12, 'Olen Sayang', 'Olenku@gmail.com', 'default.jpg', '$2y$10$Q.zEH6aQY5Fx1ppY8XejWOWR9qFV9sOI.xnOYz7TkUt3wksFI0OLu', 2, 1, 1561797944, 'Olen Sayang.png'),
(13, 'irene', 'irene@gmail.com', 'default.jpg', '$2y$10$FMnxPdOi2KhXQud6BBZ2gOuz2Xk62076wc3gbNkspT0fksLIc1Nkq', 2, 1, 1561798380, 'irene.png'),
(14, 'Suelgi', 'hadimalvin@gmail.com', 'default.jpg', '$2y$10$O3sFDRDmba7tmsYaKughM.kSTDUnHUyrHpXcRzaJ/zfnv/6oq2tsC', 2, 1, 1561804187, 'Suelgi.png'),
(15, 'Yosia', 'enriqueyosia@gmail.com', 'default.jpg', '$2y$10$YYE13lzpYXQ3ZqBbfwqrc.uyVzouS302qL/ie32ALZi.Hy1PUpWi2', 2, 1, 1561987649, 'Yosia.png'),
(16, 'hahaha', 'likevalencia2@gmail.com', 'default.jpg', '$2y$10$WiEx7.SUQSMwwa4ljUv5bOJpowM80BLut5DN8uxEfvUQoi4D8Hlpm', 2, 1, 1562037166, 'hahaha.png'),
(17, 'Daffa Prayoga', 'daffakontol@gmail.com', 'default.jpg', '$2y$10$eceZCuDXTZuYdRUxiZ4qmOqFLknnOFgCaoSCS0X0ieKD7CmX5RHYC', 2, 1, 1562062353, 'Daffa Prayoga.png');

-- --------------------------------------------------------

--
-- Table structure for table `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(4, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_menu`
--

INSERT INTO `user_menu` (`id`, `menu`) VALUES
(1, 'Admin'),
(2, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(4, 3, 'Menu Management', 'menu', 'fas fa-fw fa-folder', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
